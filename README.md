You will need the game library "lux" and the ascii art library
"raart", which you can install like:

```
$ raco pkg install lux raart
```

To start playing the graphical version, run:

```
$ racket racktris.rkt
```

For the console version, use `-c`:

```
$ racket racktris.rkt -c
```
